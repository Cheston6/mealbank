import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MealComponent } from './components/meal/meal.component';
import { HomeComponent } from './components/home/home.component';
import { CategoryComponent } from './components/category/category.component';
import { AreaComponent } from './components/area/area.component';
import { IngredientComponent } from './components/ingredient/ingredient.component';

const routes: Routes = [
  { path: 'meal', component: MealComponent },
  { path: 'category', component: CategoryComponent },
  { path: 'area', component: AreaComponent },
  { path: 'ingredient', component: IngredientComponent },
  { path: '', component: HomeComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
