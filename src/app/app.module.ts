import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { HttpClientModule } from '@angular/common/http';
import { TitleComponent } from './components/title/title.component';
import { SearchComponent } from './components/search/search.component';
import { RandomComponent } from './components/random/random.component';
import { MealComponent } from './components/meal/meal.component';
import { SafePipe } from './pipes/safe.pipe';
import { CategoryComponent } from './components/category/category.component';
import { AreaComponent } from './components/area/area.component';
import { IngredientComponent } from './components/ingredient/ingredient.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    TitleComponent,
    SearchComponent,
    RandomComponent,
    MealComponent,
    SafePipe,
    CategoryComponent,
    AreaComponent,
    IngredientComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
