import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal.service';
import { DomSanitizer } from '@angular/platform-browser';
import { Router } from '@angular/router';

@Component({
  selector: 'app-random',
  templateUrl: './random.component.html',
  styleUrls: ['./random.component.scss']
})
export class RandomComponent implements OnInit {

  constructor(
    private mealService: MealService,
    public sanitizer: DomSanitizer,
    private router: Router
  ) { }

  meals = [];
  videos = [];

  ngOnInit() {
    for (let i = 0; i < 6; i++) {
      this.mealService.getRandomMeal().subscribe(data => {
        data.meals[0].style1 = this.randomColor();
        data.meals[0].style2 = this.randomColor();
        this.meals.push(data.meals[0]);
        const list = data.meals[0].strYoutube.split('=');
        this.videos.push('https://www.youtube.com/embed/' + list[1] + '?autoplay=1&enablejsapi=1&mute=1');
      });
    }
  }

  randomColor() {
    const randomInt = (min, max) => {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    const h = randomInt(0, 360);
    const s = randomInt(42, 98);
    const l = randomInt(40, 90);
    return `hsl(${h},${s}%,${l}%)`;
  }

  randomClick(meal) {
    this.router.navigateByUrl('/meal?id=' + meal.idMeal);
  }

  filterCategory(meal) {
    this.router.navigateByUrl('/category?name=' + meal.strCategory);
  }

  filterArea(meal) {
    this.router.navigateByUrl('/area?name=' + meal.strArea);
  }

}
