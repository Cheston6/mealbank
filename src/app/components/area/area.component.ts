import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal.service';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-area',
  templateUrl: './area.component.html',
  styleUrls: ['./area.component.scss']
})
export class AreaComponent implements OnInit {

  constructor(
    private mealService: MealService,
    private router: ActivatedRoute,
    private route: Router
  ) { }

  name;
  areas;

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.name = params.name;
      this.mealService.filterArea(this.name).subscribe(data => {
        this.areas = data.meals;
      });
    });
  }

  clickArea(area) {
    this.route.navigateByUrl('/meal?id=' + area.idMeal);
  }
}
