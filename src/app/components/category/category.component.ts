import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-category',
  templateUrl: './category.component.html',
  styleUrls: ['./category.component.scss']
})
export class CategoryComponent implements OnInit {

  constructor(
    private mealService: MealService,
    private router: ActivatedRoute,
    private route: Router
  ) { }

  name;
  categorys;

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.name = params.name;
      this.mealService.filterCategory(this.name).subscribe(data => {
        this.categorys = data.meals;
      });
    });
  }

  clickCategory(category) {
    this.route.navigateByUrl('/meal?id=' + category.idMeal);
  }
}
