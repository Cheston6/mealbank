import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MealService } from 'src/app/services/meal.service';

@Component({
  selector: 'app-meal',
  templateUrl: './meal.component.html',
  styleUrls: ['./meal.component.scss']
})
export class MealComponent implements OnInit {

  constructor(
    private mealService: MealService,
    private router: ActivatedRoute,
    private route: Router
  ) { }

  id;
  meal;
  video;

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.id = params.id;
      this.mealService.getMeal(this.id).subscribe(data => {
        data.meals[0].style1 = this.randomColor();
        data.meals[0].style2 = this.randomColor();
        this.meal = data.meals[0];
        const list = data.meals[0].strYoutube.split('=');
        this.video = 'https://www.youtube.com/embed/' + list[1] + '?autoplay=1&enablejsapi=1';
      });
    });
  }

  randomColor() {
    const randomInt = (min, max) => {
      return Math.floor(Math.random() * (max - min + 1)) + min;
    };

    const h = randomInt(0, 360);
    const s = randomInt(42, 98);
    const l = randomInt(40, 90);
    return `hsl(${h},${s}%,${l}%)`;
  }

  filterCategory(meal) {
    this.route.navigateByUrl('/category?name=' + meal.strCategory);
  }

  filterArea(meal) {
    this.route.navigateByUrl('/area?name=' + meal.strArea);
  }

  filterIngredient(ingredient) {
    this.route.navigateByUrl('/ingredient?name=' + ingredient);
  }

}
