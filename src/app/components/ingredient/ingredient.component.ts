import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal.service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-ingredient',
  templateUrl: './ingredient.component.html',
  styleUrls: ['./ingredient.component.scss']
})
export class IngredientComponent implements OnInit {

  constructor(
    private mealService: MealService,
    private router: ActivatedRoute,
    private route: Router
  ) { }

  name;
  ingredients;

  ngOnInit() {
    this.router.queryParams.subscribe(params => {
      this.name = params.name;
      this.mealService.filterIngredient(this.name).subscribe(data => {
        this.ingredients = data.meals;
      });
    });
  }

  clickIngredient(ingredient) {
    this.route.navigateByUrl('/meal?id=' + ingredient.idMeal);
  }

}
