import { Component, OnInit } from '@angular/core';
import { MealService } from 'src/app/services/meal.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

  constructor(
    private mealService: MealService,
    private router: Router
  ) { }

  input = '';
  meals = [];

  ngOnInit() {
  }

  search(event) {
    setTimeout(() => {
      this.mealService.searchMeal(event.target.value).subscribe(data => {
          let index = 5;
          if (data.meals.length < 5) {
            index = data.meals.length;
          }
          this.meals = data.meals.slice(0, index);
          if (event.target.value === '') {
            this.meals = [];
          }
      });
    }, 300);
  }

  suggestion(event, meal) {
    this.meals = [];
    this.input = event.target.innerText;
    this.router.navigateByUrl('/meal?id=' + meal.idMeal);
  }
}
