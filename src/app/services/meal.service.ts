import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MealService {

  constructor(
    private http: HttpClient
  ) { }

  private searchMealUrl = 'https://www.themealdb.com/api/json/v1/1/search.php?s=';
  private getMealUrl = 'https://www.themealdb.com/api/json/v1/1/lookup.php?i=';
  private getRandomMealUrl = 'https://www.themealdb.com/api/json/v1/1/random.php';
  private getCategoriesUrl = 'https://www.themealdb.com/api/json/v1/1/categories.php';
  private filterAreaUrl = 'https://www.themealdb.com/api/json/v1/1/filter.php?a=';
  private filterIngredientUrl = 'https://www.themealdb.com/api/json/v1/1/filter.php?i=';
  private filterCategoryUrl = 'https://www.themealdb.com/api/json/v1/1/filter.php?c=';
  private listAreaUrl = 'https://www.themealdb.com/api/json/v1/1/list.php?a=list';
  private listIngredientUrl = 'https://www.themealdb.com/api/json/v1/1/list.php?i=list';
  private listCategoryUrl = 'https://www.themealdb.com/api/json/v1/1/list.php?c=list';

  public searchMeal(name: string): Observable<any> {
    return this.http.get<any>(this.searchMealUrl + name);
  }

  public getMeal(id: string): Observable<any> {
    return this.http.get<any>(this.getMealUrl + id);
  }

  public getRandomMeal(): Observable<any> {
    return this.http.get<any>(this.getRandomMealUrl);
  }

  public getCategories(): Observable<any> {
    return this.http.get<any>(this.getCategoriesUrl);
  }

  public filterArea(area: string): Observable<any> {
    return this.http.get<any>(this.filterAreaUrl + area);
  }

  public filterIngredient(ingredient: string): Observable<any> {
    return this.http.get<any>(this.filterIngredientUrl + ingredient);
  }

  public filterCategory(category: string): Observable<any> {
    return this.http.get<any>(this.filterCategoryUrl + category);
  }

  public listArea(area: string): Observable<any> {
    return this.http.get<any>(this.filterAreaUrl + area);
  }

  public listIngredient(ingredient: string): Observable<any> {
    return this.http.get<any>(this.filterIngredientUrl + ingredient);
  }

  public listCategory(category: string): Observable<any> {
    return this.http.get<any>(this.filterCategoryUrl + category);
  }

}
